/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 15;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 15;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 1;        /* 1 means no outer gap when there is only one window */
static const int swallowfloating    = 0;
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int usealtbar          = 1;
static const char *altbarclass      = "Polybar"; /* Alternate bar class name */
static const char *alttrayname      = "tray";    /* Polybar tray instance name */
static const char *altbarcmd        = "$HOME/.config/polybar/barlap.sh"; /* Alternate bar launch command **/
static const char *fonts[]          = { "Ubuntu Mono:size=10" };
static const char dmenufont[]       = "Ubuntu Mono:size=10";
static const char col_gray1[]       = "#4c566a";
static const char col_gray2[]       = "#434C5E";
static const char col_gray3[]       = "#3B4252";
static const char col_gray4[]       = "#2E3440";
static const char col_cyan[]        = "#D8DEE9";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

static const char *const autostart[] = {
	"picom", "--experimental-backends", NULL, 
  "dunst", NULL,
	"nm-applet", NULL,
	"~/.fehbg",  NULL,
  "light-locker", NULL,
	
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",    NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "brave",   NULL,     NULL,           0,         0,          0,          -1,        -1 },
	{ "St",      NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "feh",     NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
#include "movestack.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALT Mod1Mask
#define CTRL ControlMask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = {"dmenu_run", "-o", "85", "-p", "Run:", "-c", "-t", "-h", "30", "-l", "20", "-g", "3", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *browser[]  = { "librewolf", NULL };
static const char *calc[]  = { "st", "bc", NULL };



static Key keys[] = {
	/* modifier                     key        function        argument */
  /* programs */
	{ MODKEY|ALT,                   XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ALT,                   XK_e,      spawn,          SHCMD("st ranger") },
	{ MODKEY|ALT,                   XK_c,      spawn,          {.v = calc } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
  { MODKEY|ALT,                   XK_b,      spawn,          {.v = browser } },
  { MODKEY|ALT,                   XK_n,      spawn,          SHCMD("st newsboat") },
  { MODKEY|ALT,                   XK_f,      spawn,          SHCMD("ripcord") },
  { MODKEY|ALT,                   XK_m,      spawn,          SHCMD("tutanota-desktop") },
  { MODKEY|ALT,                   XK_s,      spawn,          SHCMD("steam") },
  { MODKEY|ALT,                   XK_t,      spawn,          SHCMD("st htop") },
  { MODKEY|ALT,                   XK_g,      spawn,          SHCMD("st gomuks") },
  { MODKEY|ALT,                   XK_x,      spawn,          SHCMD("multimc") },
  { MODKEY|ALT,                   XK_p,      spawn,          SHCMD("st -c cmus cmus") },
  { MODKEY|ALT,                   XK_l,      spawn,          SHCMD("lowriter --nologo") },
  { MODKEY|ALT,                   XK_w,      spawn,          SHCMD("$HOME/BashScripts/connman-wifi-dmenu") },
  { MODKEY|ALT,                   XK_v,      spawn,          SHCMD("lutris") },
  { MODKEY|ALT,                   XK_o,      spawn,          SHCMD("$HOME/BashScripts/quicksearch") },
  { MODKEY|ALT,                   XK_u,      spawn,          SHCMD("st ssh -p 2269 rawhav0kk@mcserver") },
  { MODKEY|ALT,                   XK_y,      spawn,          SHCMD("st pipe-viewer") },
  { MODKEY|ALT,                   XK_r,      spawn,          SHCMD("pavucontrol") },
  /*movement */
  { MODKEY|CTRL,                  XK_j,      aspectresize,   {.i = +24} },
  { MODKEY|CTRL,                  XK_k,      aspectresize,   {.i = -24} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = -0.05} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
  { MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
  { MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_space,  zoom,           {0} },
  { MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_x,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_Return, togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
  { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
  /* layouts*/
	{ MODKEY|ALT,                   XK_1,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ALT,                   XK_2,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ALT,                   XK_3,      setlayout,      {.v = &layouts[10]} },
	{ MODKEY|ALT,                   XK_4,      setlayout,      {.v = &layouts[11]} },
	{ MODKEY|ALT,                   XK_5,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY|ALT,                   XK_6,      setlayout,      {.v = &layouts[6]} },
	{ MODKEY|ALT,                   XK_7,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_space,  setlayout,      {0} },
  /* tags */
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
  { NULL,                         XK_Print,   spawn,        SHCMD("$HOME/BashScripts/screenshot.sh") },
  { MODKEY|ShiftMask,             XK_Print,   spawn,        SHCMD("$HOME/BashScripts/pintoscreen") },
  { MODKEY,                       XK_Print,   spawn,        SHCMD("$HOME/BashScripts/selective_screenshot.sh") },
  { NULL,                         0x1008ff12, spawn,        SHCMD("pactl set-sink-mute @DEFAULT_SINK@ toggle") },
  { NULL,                         0x1008ff13, spawn,        SHCMD("pactl set-sink-volume @DEFAULT_SINK@ +5%") },
  { NULL,                         0x1008ff11, spawn,        SHCMD("pactl set-sink-volume @DEFAULT_SINK@ -5%") },
  { NULL,                         0x1008ffb2, spawn,        SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
  { NULL,                         0x1008ff03, spawn,        SHCMD("xbacklight -dec 2 -fps 60") },
  { NULL,                         0x1008ff02, spawn,        SHCMD("xbacklight -inc 2 -fps 60") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

